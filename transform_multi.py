import sys
import images
import transforms

def main():
    archivo = sys.argv[1]
    imagen = images.read_img(archivo)

    if "/" in archivo:
        i = archivo.split("/")
        nombre, formato = i[-1].split(".")
    else:
        nombre, formato = archivo.split(".")

    for i in range(2, len(sys.argv)):
        try:
            int(sys.argv[i])
        except:
            if sys.argv[i] == "rotate_right":
                imagen = transforms.rotate_right(imagen)
            elif sys.argv[i] == "mirror":
                imagen = transforms.mirror(imagen)
            elif sys.argv[i] == "blur":
                imagen = transforms.blur(imagen)
            elif sys.argv[i] == "grayscale":
                imagen = transforms.grayscale(imagen)
            elif sys.argv[i] == "crop":
                imagen = transforms.crop(imagen, int(sys.argv[i + 1]), int(sys.argv[i + 2]), int(sys.argv[i + 3]), int(sys.argv[i + 4]))
            elif sys.argv[i] == "shift":
                imagen = transforms.shift(imagen, int(sys.argv[i + 1]), int(sys.argv[i + 2]))
            elif sys.argv[i] == "change_colors":
                imagen = transforms.change_colors(imagen, (int(sys.argv[i + 1]), int(sys.argv[i + 2]), int(sys.argv[i + 3])), (int(sys.argv[i + 4]), int(sys.argv[i + 5]), int(sys.argv[i + 6])))
            elif sys.argv[i] == "filter":
                imagen = transforms.filter(imagen, float(sys.argv[i + 1]), float(sys.argv[i + 2]), float(sys.argv[i + 3]))
            elif sys.argv[i] == "rotate_colors":
                imagen = transforms.rotate_colors(imagen, int(sys.argv[i + 1]))

    images.write_img(imagen, f"../{nombre}_trans.{formato}")

if __name__ == '__main__':
    main()